import { of } from 'rxjs';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { cold } from '@nrwl/angular/testing';

import { ProductService } from './product.service';

describe('ProductService', () => {
  let service: ProductService;
  let http: HttpClient;

  beforeEach(() => {
    const httpStub = { get: jest.fn(() => of([])) };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        {
          provide: HttpClient,
          useValue: httpStub,
        },
        ProductService,
      ],
    });

    http = TestBed.inject(HttpClient);
    // console.log('LOG http: ', JSON.stringify(http));
    service = TestBed.inject(ProductService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve all products', () => {
    const expected = cold('-a|', { a: [] });
    http.get = jest.fn(() => expected);

    expect(service.getAll()).toBeObservable(expected);
    expect(http.get).toHaveBeenCalledTimes(1);
  });
});
