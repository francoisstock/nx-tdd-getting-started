import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ProductsEffects } from './+state/products.effects';
import { ProductsFacade } from './+state/products.facade';
import * as fromProducts from './+state/products.reducer';
import { PRODUCTS_FEATURE_KEY } from './+state/products.state';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(PRODUCTS_FEATURE_KEY, fromProducts.reducer),
    EffectsModule.forFeature([ProductsEffects]),

    HttpClientModule,
  ],
  providers: [ProductsFacade],
})
export class ProductsDataAccessModule {}
