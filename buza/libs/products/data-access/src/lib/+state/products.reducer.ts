import { NewLocalId, Product } from '@buza/products/domain';
import { Action, createReducer, on } from '@ngrx/store';

import * as ProductsActions from './products.actions';
import { initialState, productsAdapter, State } from './products.state';

const productsReducer = createReducer(
  initialState,
  on(ProductsActions.loadProducts, (state) => ({
    ...state,
    isLoaded: false,
    error: null,
  })),
  on(ProductsActions.loadProductsSuccess, (state, { products }) =>
    productsAdapter.setAll(products, { ...state, isLoaded: true })
  ),
  on(ProductsActions.loadProductsFailure, (state, { error }) => ({
    ...state,
    isLoaded: false,
    error,
  })),

  on(ProductsActions.toggleCodeVisibility, (state, { isVisible }) => ({
    ...state,
    isCodeVisible: isVisible,
  })),

  on(ProductsActions.setCurrentProduct, (state, {product}) => ({
    ...state,
    selectedId: product.id
  })),
  on(ProductsActions.clearCurrentProduct, state => ({
    ...state,
    selectedId: null
  })),
  on(ProductsActions.initializeNewProduct, (state) => ({
    ...state,
    selectedId: NewLocalId,
  })),
  on(ProductsActions.createNewProduct, (state, { product }) => {
    const newProduct = Product.create(product);
    return productsAdapter.addOne(newProduct, {
      ...state,
      selectedId: newProduct.id,
    });
  }),
  on(ProductsActions.updateProduct, (state, {product}) =>
    productsAdapter.setOne(product,{
      ...state,
      selectedId : product.id
    })
  ),
  on(ProductsActions.deleteProduct, (state, {product}) =>
    productsAdapter.removeOne(product.id,{
      ...state,
      selectedId: null
    })
  )
);

export function reducer(state: State | undefined, action: Action) {
  return productsReducer(state, action);
}
