import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';

import * as ProductsSelectors from './products.selectors';
import { ProductsPartialState } from './products.state';

@Injectable()
export class ProductsFacade {
  public readonly loaded$ = this.store.pipe(select(ProductsSelectors.getProductsLoaded));
  public readonly allProducts$ = this.store.pipe(select(ProductsSelectors.getAllProducts));
  public readonly selectedProducts$ = this.store.pipe(select(ProductsSelectors.getSelected));

  public constructor(private store: Store<ProductsPartialState>) {}

  /*private*/ dispatch(action: Action) {
    this.store.dispatch(action);
  }
}
