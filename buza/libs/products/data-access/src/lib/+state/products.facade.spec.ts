import { of } from 'rxjs';

import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Product } from '@buza/products/domain';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { NxModule } from '@nrwl/angular';
import { readFirst } from '@nrwl/angular/testing';

import { ProductService } from '../product.service';
import * as ProductsActions from './products.actions';
import { ProductsEffects } from './products.effects';
import { ProductsFacade } from './products.facade';
import { reducer } from './products.reducer';
import { PRODUCTS_FEATURE_KEY, State } from './products.state';

interface TestSchema {
  products: State;
}

describe('ProductsFacade', () => {
  let facade: ProductsFacade;
  let store: Store<TestSchema>;
  const createProductsEntity = (id: number, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
      code: `code-${id}`,
      description: `description-${id}`,
      starRating: 3,
    } as Product);
  const products = [
    createProductsEntity(1),
    createProductsEntity(2),
    createProductsEntity(3)
  ]

  beforeEach(() => { });

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature(PRODUCTS_FEATURE_KEY, reducer),
          EffectsModule.forFeature([ProductsEffects]),
        ],
        providers: [ProductsFacade,
          {
            provide: ProductService, useValue: { getAll: jest.fn() }
          }]
      })
      class CustomFeatureModule { }

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule,
        ],
      })
      class RootModule { }
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.inject(Store);
      facade = TestBed.inject(ProductsFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async (done) => {
      try {
        let list = await readFirst(facade.allProducts$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.dispatch(ProductsActions.loadProducts());

        list = await readFirst(facade.allProducts$);
        isLoaded = await readFirst(facade.loaded$);

        // expect(list.length).toBe(3);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `loadProductsSuccess` to manually update list
     */
    it('allProducts$ should return the loaded list; and loaded flag == true', async (done) => {
      try {
        let list = await readFirst(facade.allProducts$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.dispatch(
          ProductsActions.loadProductsSuccess({
            products: [
              createProductsEntity(1),
              createProductsEntity(2),
            ],
          })
        );

        list = await readFirst(facade.allProducts$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(2);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });
}); 