import { Observable, of } from 'rxjs';

import { async, TestBed } from '@angular/core/testing';
import { Product } from '@buza/products/domain';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { cold, hot } from '@nrwl/angular/testing';

import { ProductService } from '../product.service';
import * as ProductsActions from './products.actions';
import { ProductsEffects } from './products.effects';

describe('ProductsEffects', () => {
  let actions$: Observable<any>;
  let effects: ProductsEffects;
  let service: ProductService;

  const createProductsEntity = (id: number, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
      code: `code-${id}`,
      description: `description-${id}`,
      starRating: 3,
    } as Product);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        { provide: ProductService, useValue: { getAll: jest.fn() } },

        ProductsEffects,
        DataPersistence,
        provideMockActions(() => actions$),
        provideMockStore(),
      ],
    });

    effects = TestBed.inject(ProductsEffects);
    service = TestBed.inject(ProductService);
  });

  describe('loadProducts$', () => {
    const products = [
      createProductsEntity(1),
      createProductsEntity(2),
      createProductsEntity(3)
    ];

    const loadAction = ProductsActions.loadProducts();
    describe('loadProductsSuccess', () => {
      it('should contain products', () => {
        actions$ = hot('-a-|', { a: loadAction });
        const expected$ = cold('--a|', {
          a:
            ProductsActions.loadProductsSuccess({ products })
        });

        service.getAll = jest.fn(() =>
          cold('-a|', { a: products })
        );

        expect(effects.loadProducts$).toBeObservable(expected$);
      });
    });

    describe('loadProductsFailure', () => {
      it('should contain the error', () => {
        actions$ = hot('-a-|', { a: loadAction });
        const errorMessage = 'something went wrong';
        const expected$ = cold('--a|', {
          a: ProductsActions.loadProductsFailure({ error: errorMessage })
        });

        service.getAll = jest.fn(() => 
          cold('-#|', {}, errorMessage)
        );

        expect(effects.loadProducts$).toBeObservable(expected$);
      });
    });
  });
});
