import { map } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';

import { ProductService } from '../product.service';
import * as ProductsActions from './products.actions';
import * as fromProducts from './products.reducer';
import { initialState } from './products.state';

@Injectable()
export class ProductsEffects {
  public readonly loadProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProductsActions.loadProducts),
      fetch({
        run: (action) => {
          return this.service.getAll().pipe(
            map(products => ProductsActions.loadProductsSuccess({products}))
          )
        },

        onError: (action, error) => {
          return ProductsActions.loadProductsFailure({ error });
        },
      })
    )
  );

  public constructor(
    private readonly actions$: Actions, 
    private readonly service: ProductService
  ) { /**/ }
}
