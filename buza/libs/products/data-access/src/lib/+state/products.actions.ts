import { Product } from '@buza/products/domain';
import { createAction, props } from '@ngrx/store';

// List
export const loadProducts = createAction('[Products] Load Products');

export const loadProductsSuccess = createAction(
  '[Products] Load Products Success',
  props<{ products: Product[] }>()
);

export const loadProductsFailure = createAction(
  '[Products] Load Products Failure',
  props<{ error: any }>()
);

// Toggle
export const toggleCodeVisibility = createAction(
  '[Products] Toogle Product Code',
  props<{ isVisible: boolean }>()
);

// Create
export const initializeNewProduct = createAction(
  '[Products] Initialize New Product'
);

export const createNewProduct = createAction(
  '[Products] Create New Product',
  props<{ product: Product }>()
);

export const createNewProductSuccess = createAction(
  '[Products] Create New Product Success',
  props<{ product: Product }>()
);
export const createNewProductFailure = createAction(
  '[Products] Create New Product Failure',
  props<{ error: any }>()
);

// Update
export const updateProduct = createAction(
  '[Products] Update Product',
  props<{ product: Product }>()
);

export const updateProductSuccess = createAction(
  '[Products] Update Product Success',
  props<{ product: Product }>()
);

export const updateProductFailure = createAction(
  '[Products] Update Product Failure',
  props<{ error: any }>()
);

// Delete
export const deleteProduct = createAction(
  '[Products] Delete product',
  props<{ product: Product }>()
);

export const deleteProductSuccess = createAction(
  '[Products] Delete Product Success'
);

export const deleteProductFailure = createAction(
  '[Products] Delete Product Failure',
  props<{ error: any }>()
);

export const setCurrentProduct = createAction(
  '[Products] Set Current Product',
  props<{ product: Product }>()
);

export const clearCurrentProduct = createAction(
  '[Products] Clear Current Product'
);
