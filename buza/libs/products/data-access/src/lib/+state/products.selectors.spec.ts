import { Product } from '@buza/products/domain';

import * as ProductsSelectors from './products.selectors';
import { initialState, productsAdapter, State } from './products.state';

describe('Products Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getProductsId = (it) => it['id'];
  const createProductsEntity = (id: number, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
      code: `code-${id}`,
      description: `description-${id}`,
      starRating: 3,
    } as Product);

  let state;

  beforeEach(() => {
    state = {
      products: productsAdapter.setAll(
        [
          createProductsEntity(1),
          createProductsEntity(2),
          createProductsEntity(3),
        ],
        {
          ...initialState,
          selectedId: 2,
          error: ERROR_MSG,
          loaded: true,
          isCodeVisible: true
        }
      ),
    };
  });

  describe('Products Selectors', () => {
    it('getAllProducts() should return the list of Products', () => {
      const results = ProductsSelectors.getAllProducts(state);
      const selId = getProductsId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe(2);
    });

    it('getSelectedId() should return the selected Entity Id', () =>  {
      const result = ProductsSelectors.getSelectedId(state);
      expect(result).toBe(2);
    })

    it('getSelected() should return the selected Entity', () => {
      const result = ProductsSelectors.getSelected(state);
      const selId = getProductsId(result);

      expect(selId).toBe(2);
    });

    it("getProductsLoaded() should return the current 'loaded' status", () => {
      const result = ProductsSelectors.getProductsLoaded(state);

      expect(result).toBe(false);
    });

    it("getProductsError() should return the current 'error' state", () => {
      const result = ProductsSelectors.getProductsError(state);

      expect(result).toBe(ERROR_MSG);
    });

    it('getIsCodeVisible() should return the current \'isCodeVisible\' state', () => {
      const result = ProductsSelectors.getIsCodeVisible(state);
      expect(result).toBe(true);
    })
  });
});
