import { NewLocalId, Product } from '@buza/products/domain';

import * as ProductsActions from './products.actions';
import { reducer } from './products.reducer';
import { initialState, State, productsAdapter } from './products.state';
import { act } from '@ngrx/effects';
import { CANCELLED } from 'dns';

describe('Products Reducer', () => {
  const createProductsEntity = (id: number, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
      code: `code-${id}`,
      description: `description-${id}`,
      starRating: 3,
    } as Product);

  beforeEach(() => {});

  describe('loadProducts', () => {
    it('loadProducts should return loaded false', () => {
      const action = ProductsActions.loadProducts();

      const result = reducer(initialState, action);

      expect(result.isLoaded).toBe(false);
      expect(result.error).toBeFalsy();
    });
  });

  describe('loadProductsSuccess', () => {
    it('loadProductsSuccess should return the list of known Products', () => {
      const products = [createProductsEntity(1), createProductsEntity(9)];
      const action = ProductsActions.loadProductsSuccess({ products });

      const result = reducer(initialState, action);

      expect(result.isLoaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('loadProductsFailure', () => {
    it('loadProductsFailure should return error and empty list of products', () => {
      const error = 'dummy error';
      const action = ProductsActions.loadProductsFailure({
        error,
      });

      const result = reducer(initialState, action);

      expect(result.isLoaded).toBe(false);
      expect(result.ids.length).toBe(0);
      expect(result.error).toBe(error);
    });
  });

  describe('toggleCodeVisibility', () => {
    it('toggleCodeVisibility should toggle visibility', () => {
      const visible = true;
      const action = ProductsActions.toggleCodeVisibility({
        isVisible: visible,
      });

      const result = reducer(initialState, action);

      expect(result.isCodeVisible).toBe(visible);
    });
  });

  describe('initializeNewProduct', () => {
    it('initializeNewProduct should initialize product creation', () => {
      const action = ProductsActions.initializeNewProduct();

      const result = reducer(initialState, action);

      expect(result.selectedId).toBe(0);
    });
  });

  describe('createNewProduct', () => {
    const irrelevantId = 0;
    const product = createProductsEntity(irrelevantId);
    const result: State = (() => {
      const action = ProductsActions.createNewProduct({ product });
      return reducer(initialState, action);
    })();

    it('should create product', () => {
      const createdProduct = createProductsEntity(irrelevantId);
      createdProduct.id = NewLocalId;
      expect(result.entities[NewLocalId]).toEqual(createdProduct);
    });

    it('should select created product', () => {
      expect(result.selectedId).toBe(NewLocalId);
    });

    it('should create product with new local id', () => {
      expect(result.ids).toContain(NewLocalId);
    });
  });

  describe('updateProduct', () => {
    const productId = 2;
    const updatedName = 'updated name'
    const product = createProductsEntity(productId);
    const result: State = (() => {
      const state = productsAdapter.addOne(product, initialState);
      const newProduct = createProductsEntity(productId)
      newProduct.name = updatedName;
      const action = ProductsActions.updateProduct({product: newProduct});
     return reducer(state,action);
    })();

    it('should update the product', () => {
      expect(result.entities[productId].name).toBe(updatedName);
    });

    it('should select the updated product', () => {
      expect(result.selectedId).toBe(productId);
    });
  });

  describe('DeleteProduct', () => {
    const productId = 2;
    const product = createProductsEntity(productId);
    const result: State = (() => {
      const state = productsAdapter.addOne(product,{
        ...initialState,
        selectedId: productId
      });
      const action = ProductsActions.deleteProduct({product});
      return reducer(state, action);
    })();

    it('should delete the product from state', () => {
      expect(result.entities[productId]).toBeFalsy();
    });

    it('it should set selectid to null', () => {
      expect(result.selectedId).toBeFalsy();
    });
  });

  describe('setCurrentProduct', () => {
    const productId = 2;
    const product = createProductsEntity(productId);
    let state = productsAdapter.addOne(product,initialState);

    it('it should select the current product', () =>{
      const action = ProductsActions.setCurrentProduct({product});
      const result = reducer(state, action);

      expect(result.selectedId).toBe(productId);
    });

    it('should clear the current product', () => {
      state = {...state, selectedId: productId};
      const action = ProductsActions.clearCurrentProduct();
      const result = reducer(state, action);

      expect(result.selectedId).toBeFalsy();
    });
  })

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
