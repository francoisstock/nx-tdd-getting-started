import { Product } from '@buza/products/domain';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const PRODUCTS_FEATURE_KEY = 'products';

export interface State extends EntityState<Product> {
  selectedId?: string | number; // which Products record has been selected
  isLoaded: boolean; // has the Products list been loaded
  error?: string | null; // last none error (if any)
  isCodeVisible: boolean;
}

export interface ProductsPartialState {
  readonly [PRODUCTS_FEATURE_KEY]: State;
}

export const productsAdapter: EntityAdapter<Product> = createEntityAdapter<
  Product
>();

export const initialState: State = productsAdapter.getInitialState({
  isLoaded: false,
  isCodeVisible: false,
});
