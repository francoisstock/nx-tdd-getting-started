

import { Observable, of } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '@buza/products/domain';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private readonly baseUrl = 'api/products';

  public constructor(private readonly http: HttpClient) {
    /**/
  }

  public getAll(): Observable<Product[]> {
  return this.http.get<Product[]>(`${this.baseUrl}`);
  }
}
