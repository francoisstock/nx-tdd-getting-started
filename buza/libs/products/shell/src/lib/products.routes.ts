import { Routes } from '@angular/router';

import { ProductShellComponent } from './containers/product-shell/product-shell.component';

export const ROUTES: Routes = [{ path: '', component: ProductShellComponent }];
