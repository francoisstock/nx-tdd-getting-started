import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ProductShellComponent } from './containers/product-shell/product-shell.component';
import { ROUTES } from './products.routes';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  declarations: [ProductShellComponent],
})
export class ProductsShellModule {}
