export class Product {
  id: number | null;
  name: string;
  code: string;
  description: string;
  starRating: number;

  /** Creates a product with ID 0: not yet been saved by API */
  static create(product: Product): Product {
    return { ...product, id: NewLocalId };
  }
}

export const NewLocalId = 0;
