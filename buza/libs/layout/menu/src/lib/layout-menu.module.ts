import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';

import { MenuComponent } from './menu/menu.component';

@NgModule({
  imports: [CommonModule, MatToolbarModule],
  declarations: [MenuComponent],
  exports: [MenuComponent]
})
export class LayoutMenuModule {}
