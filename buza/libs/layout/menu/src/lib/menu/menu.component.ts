import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'buza-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() public appTitle: string;

  constructor() { }

  ngOnInit(): void {
  }

}
