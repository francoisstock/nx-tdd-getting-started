module.exports = {
  name: 'layout-shell',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/layout/shell',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
