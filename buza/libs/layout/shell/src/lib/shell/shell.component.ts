import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'buza-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {
  @Input() public appTitle: string;

  constructor() { }

  ngOnInit(): void {
  }

}
