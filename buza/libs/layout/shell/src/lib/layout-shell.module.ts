import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutMenuModule } from '@buza/layout/menu';

import { ShellComponent } from './shell/shell.component';

@NgModule({
  imports: [CommonModule, LayoutMenuModule, RouterModule],
  declarations: [ShellComponent],
  exports: [ShellComponent]
})
export class LayoutShellModule {}
