export const getPageTitle = () => cy.get('mat-toolbar[color=primary] > span');
// nav.navbar.navbar-expand-lg > div.container-fluid > a.navbar-brand')

export const getGreeting = () => cy.get('div.card-header');

export const getProducts = () => cy.get('li.product');
export const getAddProductButton = () => cy.get('button#add-product');
