import { getAddProductButton, getGreeting, getPageTitle, getProducts } from '../support/app.po';

describe('AcmeApp', () => {
  describe('Homepage', () => {
    beforeEach(() => cy.visit('/'));

    it('should display page title', () => {
      // TODO: improve assertions
      getPageTitle().contains('Acme Product Management');
    })

    it('should display greetings', () => {
      getGreeting().contains('welcome');
    });
  });

  describe('Product tests', () => {
    beforeEach(() => cy.visit('/products'));

    it('should display products', () => {
      getProducts().should((t) => expect(t.length).equal(2));
      getAddProductButton().click();
      getProducts().should((t) => expect(t.length).equal(3));
    });
  });
});
