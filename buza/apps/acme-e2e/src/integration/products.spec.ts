import { getAddProductButton, getGreeting, getPageTitle, getProducts } from '../support/app.po';

describe('Product tests', () => {

  it('should display products', () => {
    cy.visit('/products');
    //cy.get("form");

    cy.get('p')
    .type('Mind you if I ask some silly question?')
    .should('have.value', 'Mind you if I ask some silly question?');

    // exemple for type in input

    // cy.get('input[name="name"]')
    //   .type("product uno")
    //   .should("have.value", "product uno");

    // getProducts().should((t) => expect(t.length).equal(2));
    // getAddProductButton().click();
    // getProducts().should((t) => expect(t.length).equal(3));
  });
});
