import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { environment } from '../environments/environment';
import { APP_ROUTES } from './app-routes';

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES, {
      initialNavigation: 'enabled',
      enableTracing: environment.isProduction,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
  /**/
}
