import { Routes } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome.component';

export const APP_ROUTES: Routes = [
  {
    path: '',
    component: WelcomeComponent,
  },
  {
    path: 'products',
    loadChildren: () =>
      import('@buza/products/shell').then((m) => m.ProductsShellModule),
  },
  {
    path: 'welcome',
    redirectTo: '',
    pathMatch: 'full',
  },
];
