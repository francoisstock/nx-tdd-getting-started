import { Component } from '@angular/core';

@Component({
  selector: 'buza-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public readonly appTitle = 'Acme Product Management';

  // For demo purpose
  public get passportCount(): number {
    return 3;
  }
}
