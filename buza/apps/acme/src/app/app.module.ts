import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutShellModule } from '@buza/layout/shell';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductData } from './fake-db';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  declarations: [AppComponent, WelcomeComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutShellModule,
    HttpClientModule,
    !environment.isProduction ? HttpClientInMemoryWebApiModule.forRoot(ProductData) : [],
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
